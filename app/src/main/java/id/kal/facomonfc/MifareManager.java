package id.kal.facomonfc;

import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.TagTechnology;
import android.util.Log;

import java.io.IOException;

/**
 * Created by Kristiawan on 13/11/18.
 */
public class MifareManager {
    public static final int KEY_TYPE_A = 0;
    public static final int KEY_TYPE_B = 1;

    public static final int TAG_NO_COMPATIBLE = -1;
    public static final int MIFARE_CONNECTION_SUCCESS = 0;
    public static final int MIFARE_ERROR = -2;
    public static final int MIFARE_TAG_NULL = -3;

    public static final byte[] DEFECTO_KEY = new byte[]{(byte) 0xFB, (byte) 0x012, (byte) 0xFF, (byte) 0x00, (byte) 0x0F, (byte) 0x01};
    public static final byte[] STANDAR_KEY = new byte[]{(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};

    // ***** variables de clase *****
    public byte[] buffer = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    private Tag nTag = null;
    private MifareClassic mifareClassic = null;

    /**
     * Constructor
     *
     * @param tag
     */
    public MifareManager(Tag tag) {
        mifareClassic = null;
        nTag = tag;
    }

    /**
     * Obtains The ID Tag
     *
     * @return Array of bytes with the unic ID Information contained on the Tag
     */
    public byte[] idTag() {
        return nTag.getId();
    }

    /**
     * It will try to connect only if the tag is compatible (if it belongs to MifareClassicClass)
     *
     * @return MIFARE_TAG_NULL if the object is null,
     * TAG_NO_COMPATIBLE if the tag is not compatible with Mifaree,
     * MIFARE_CONNECTION_SUCCESS for success connection.
     */

    public int connectTag() {
        int cont = 0;
        if (nTag == null) {
            return MIFARE_TAG_NULL;
        }
        String[] listaTag = nTag.getTechList();
        for (; cont < listaTag.length; cont++) {
            if (listaTag[cont].equals(MifareClassic.class.getName())) {
                break;
            }
        }
        if (cont >= listaTag.length) {
            return TAG_NO_COMPATIBLE;
        }
        mifareClassic = MifareClassic.get(nTag);
        try {
            mifareClassic.connect();
        } catch (IOException e) {
            e.printStackTrace();
            return MIFARE_ERROR;
        }
        return MIFARE_CONNECTION_SUCCESS;
    }

    public void disconnectTag() {

        try {
            mifareClassic.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Authentificate the sector access
     *
     * @param key     Arrey of bytes that contains the access key (6 bytes)
     * @param typeKey Tipo de clave; KEY_TYPE_A o KEY_TYPE_B
     * @param sector  number (0-15)
     * @return true for success
     */
    public boolean authentificationKey(byte[] key, int typeKey, int sector) {

        try {
            switch (typeKey) {
//                case KEY_TYPE_A: if(mifareClassic.authenticateSectorWithKeyA(sector, key)) return true;
                case KEY_TYPE_A:
                    if (mifareClassic.authenticateSectorWithKeyA(sector, MifareClassic.KEY_DEFAULT))
                        return true;
                case KEY_TYPE_B:
                    if (mifareClassic.authenticateSectorWithKeyB(sector, key)) return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Read a block from the especific sector
     *
     * @param sector Sector index
     * @param block  Block Index
     * @return It returns an Array of 16 bytes or null if an error ocurrs
     */
    public byte[] readMifareTagBlock(int sector, int block) {

        try {
            return mifareClassic.readBlock(block + (sector * 4));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void readMifareTagBlock() {
        for (int i = 0; i < mifareClassic.getSectorCount(); i++) {
            boolean isAuth = false;
            try {
                if (mifareClassic.authenticateSectorWithKeyA(i, MifareClassic.KEY_MIFARE_APPLICATION_DIRECTORY)
                        && mifareClassic.authenticateSectorWithKeyB(i, MifareClassic.KEY_MIFARE_APPLICATION_DIRECTORY)) {
                    isAuth = true;
                } else if (mifareClassic.authenticateSectorWithKeyA(i, MifareClassic.KEY_DEFAULT)
                        && mifareClassic.authenticateSectorWithKeyB(i, MifareClassic.KEY_DEFAULT)) {
                    isAuth = true;
                } else if (mifareClassic.authenticateSectorWithKeyA(i,MifareClassic.KEY_NFC_FORUM)
                        && mifareClassic.authenticateSectorWithKeyB(i, MifareClassic.KEY_NFC_FORUM)) {
                    isAuth = true;
                } else {
                    Log.d("TAG", "Authorization denied ");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            int bIndex;
            int bCount;

            if (isAuth) {
                Log.d(this.getClass().getSimpleName(), "Sector "+i+": verifed succesfully");
                Log.d(this.getClass().getSimpleName(), "timeout: "+mifareClassic.getTimeout());
                bCount = mifareClassic.getBlockCountInSector(i);
                bIndex = mifareClassic.sectorToBlock(i);
                Log.d(this.getClass().getSimpleName(), "isConnected : "+mifareClassic.isConnected());
                Log.d(this.getClass().getSimpleName(), "size : "+mifareClassic.getSize());
//                byte[] data = new byte[0];
//                try {
//                    data = mifareClassic.readBlock(bIndex);
//                    Log.d(this.getClass().getSimpleName(), "data : "+toHex(data));
//                    Log.d(this.getClass().getSimpleName(), "block index : "+bIndex);
//                } catch (IOException e) {
//                    Log.d(this.getClass().getSimpleName(), "data : no data");
//                }
                for (int j=0;j<bCount;j++){
                    try {
                        byte[] data = mifareClassic.readBlock(bIndex);
                        if (data != null)
                        Log.d(this.getClass().getSimpleName(), "data : "+toHex(data));
                    } catch (IOException e) {
                        Log.d(this.getClass().getSimpleName(), e.getMessage());
                        Log.d(this.getClass().getSimpleName(), "data : no data");
                    }

                    bIndex++;
                }
            } else {
                Log.d(this.getClass().getSimpleName(), "Sector "+i+": failure");
            }
        }
    }

    private String convByteToString(byte[] datos) {
        String d = "";
        byte low, high; //low 00001111, high 11110000

        for (int i = 0; i < datos.length; i++) {
            low = (byte) (datos[i] & 0x0F);
            high = (byte) ((datos[i] >> 4) & 0x0F);
            if (high != (byte) 0x0F && low != (byte) 0x0F) {
                d += String.valueOf(((datos[i] >> 4) & 0x0F)) + String.valueOf((datos[i] & 0x0F));
            } else if (high == (byte) 0x0F) {
                break;
            } else if (low == (byte) 0x0F) {
                d += String.valueOf(high);
                break;
            }

        }

        return d;
    }

    private static String digits = "0123456789abcdef";

    public String toHex(byte[] data){
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i != data.length; i++)
        {
            int v = data[i] & 0xff;
            buf.append(digits.charAt(v >> 4));
            buf.append(digits.charAt(v & 0xf));
        }
        return buf.toString();
    }

    /**
     * Write in a block from the especific sector
     *
     * @param sector  Sector Index
     * @param block   Block Index
     * @param dBuffer Array of bytes with 16 bytes of data in Hexadecimal format
     * @return It retuns true for success, false otherwise
     */
    public boolean writeMifareTag(int sector, int block, byte[] dBuffer) {

        try {
            mifareClassic.writeBlock(block + (sector * 4), dBuffer);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public void getInfoMifare() {
        Log.d(this.getClass().getSimpleName(), "size : " + mifareClassic.getSize());
        Log.d(this.getClass().getSimpleName(), "type : " + mifareClassic.getType());
        Log.d(this.getClass().getSimpleName(), "sector : " + mifareClassic.getSectorCount());
        Log.d(this.getClass().getSimpleName(), "block : " + mifareClassic.getBlockCount());
    }

    public int getBlockCountInSector(int sector) {
        return mifareClassic.getBlockCountInSector(sector);
    }
}
