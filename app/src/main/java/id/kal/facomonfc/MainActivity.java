package id.kal.facomonfc;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.charset.StandardCharsets;

public class MainActivity extends AppCompatActivity {

    private PendingIntent pendingIntent = null;
    private NfcAdapter nfcAdapter = null;
    private boolean nfcActive = false;
    private MifareManager mifareManager;

    private TextView textViewHello;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewHello = findViewById(R.id.hello);

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);

        if (nfcAdapter != null) { // si se encontro el hardware NFC en el movil...
            if (nfcAdapter.isEnabled()) {
                pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
                nfcActive = true;
            } else
                Toast.makeText(getBaseContext(), "NFC IS DISABLE!", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getBaseContext(), "THIS DEVICE DOES NOT HAVE NFC HARDWARE ISIDE", Toast.LENGTH_LONG).show();
        }
    }

    private String convByteToString(byte[] datos) {
        String d = "";
        byte low, high; //low 00001111, high 11110000

        for (int i = 0; i < datos.length; i++) {
            low = (byte) (datos[i] & 0x0F);
            high = (byte) ((datos[i] >> 4) & 0x0F);
            if (high != (byte) 0x0F && low != (byte) 0x0F) {
                d += String.valueOf(((datos[i] >> 4) & 0x0F)) + String.valueOf((datos[i] & 0x0F));
            } else if (high == (byte) 0x0F) {
                break;
            } else if (low == (byte) 0x0F) {
                d += String.valueOf(high);
                break;
            }

        }

        return d;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (nfcActive)
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, new IntentFilter[]{new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED)}, null);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Log.d(getClass().getSimpleName(), "new Intent");
        Tag nfcTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        mifareManager = new MifareManager(nfcTag);
        Log.d(this.getClass().getSimpleName(), nfcTag.toString());

        switch (mifareManager.connectTag()) {
            case MifareManager.TAG_NO_COMPATIBLE:
                Toast.makeText(getBaseContext(), "Tag not supported!", Toast.LENGTH_LONG).show();
                break;
            case MifareManager.MIFARE_CONNECTION_SUCCESS:

                String mode = getIntent().getStringExtra("mode");
                if (mode.equals("read")) {
                    mifareManager.readMifareTagBlock();
                } else if (mode.equals("write")) {
                    if (mifareManager.authentificationKey(MifareManager.STANDAR_KEY, MifareManager.KEY_TYPE_A, 2)) {

                    }
                    byte[] datas = new byte[16];
                    byte[] hello = "kristiawan.adi".getBytes(StandardCharsets.US_ASCII);
                    System.arraycopy(hello, 0, datas, 0, hello.length);

                    if (mifareManager.writeMifareTag(2, 1, datas)) {
                        Toast.makeText(getBaseContext(), "Writing success", Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        Toast.makeText(getBaseContext(), "Writing failed", Toast.LENGTH_LONG).show();
                    }
                }


//                if (mifareManager.authentificationKey(MifareManager.STANDAR_KEY, MifareManager.KEY_TYPE_A, 0)) {
//                    Toast.makeText(getBaseContext(), "autentification ok!", Toast.LENGTH_LONG).show();
//
//                    mifareManager.getInfoMifare();
//                    Log.d(this.getClass().getSimpleName(), "block in sector : "+mifareManager.getBlockCountInSector(0));
//
//                    byte datas[] = mifareManager.readMifareTagBlock(0, 0);
//                    if (datas != null) {
//                        String u = convByteToString(datas);
//                        textViewHello.setText(u);
//                    } else
//                        Toast.makeText(getBaseContext(), "no data available!", Toast.LENGTH_LONG).show();
//                }

                break;
            default:
                Toast.makeText(getBaseContext(), "Tag null", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
