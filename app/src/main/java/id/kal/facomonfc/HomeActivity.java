package id.kal.facomonfc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonRead;
    private Button buttonWrite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        buttonRead = findViewById(R.id.read);
        buttonWrite = findViewById(R.id.write);

        buttonRead.setOnClickListener(this);
        buttonWrite.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        if (v.getId() == buttonRead.getId()) {
            intent.putExtra("mode", "read");
            startActivity(intent);
        } else if (v.getId() == buttonWrite.getId()) {
            intent.putExtra("mode", "write");
            startActivity(intent);
        }
    }
}
